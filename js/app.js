Vue.component('box-game', {
    props:{
        colors: {
            type: Array,
        }
    },
    template: `
         <div class="row">
               <div class="col-md-6 offset-md-3 mt-3">
                   <template v-for="color in initColor">
                        <div @click="boxClick(color)" class="mr-2 box" :class="color"></div>
                   </template>
               </div>
               <div class="col-md-6 offset-md-3 mt-3">
                    <template v-for="color in colors">
                         <div @click="boxClick(color)" class="mr-2 box" :class="color"></div>
                    </template>
               </div>
         </div>
    `,
    data() {
        return {
            initColor: ['white', 'red']
        }
    },
    methods: {
        /**
         * Listen to box click
         * @param color Color of the box that click
         */
        boxClick(color) {
            this.$emit('box-click', color);
        }
    }
});

let app = new Vue({
    el: '#app',
    data: {
        colors: [],
        prevSelectedColor: null,
        result: false,
        message: null,
        gameFinish: false,
    },
    methods: {
        /**
         * Manipulate the boxes
         * @param color
         */
        boxClick(color) {
            if (!this.gameFinish) {
                let game = this.gameOver(color);
                let gameOver = game[0];

                if (!gameOver) {
                    // Populate the boxes.
                    if (color === 'white') {
                        if (game[1] !== null) {
                            this.colors.push('black', 'red');
                        } else {
                            this.colors.push('white', 'orange');
                        }

                    } else if (color === 'red') {
                        this.colors.push('black', 'red');

                    } else if (color === 'black' || (color === 'orange' && !this.colors.includes('green'))) {
                        this.colors.push('green', 'orange', 'black');

                    } else if (color === 'black' || (color === 'orange' && !this.colors.includes('black'))) {
                        this.colors.push('green', 'orange', 'black');

                    } else if (color === 'orange' || color === 'green') {
                        let newColors = this.colors.filter(function (e) {
                            if (e !== 'black') {
                                return e;
                            }
                        });
                        this.colors = newColors;

                    }
                    // Initialized the previous selected color.
                    this.prevSelectedColor = color;
                } else {
                    this.gameFinish = true;
                    this.message = 'Game Over. You lose! ';
                    this.result = false;
                    this.colors = [];
                    this.prevSelectedColor = null;
                }
            }
        },
        /**
         * Check if the game is over
         * @param color
         * @returns {[boolean, null]}
         */
        gameOver(color) {
            let remove = false;
            let result = false;
            let colors = null;
            // Check if the color red is click twice.
            if (this.prevSelectedColor !== null) {
                if ((this.prevSelectedColor === 'red' && color === 'red') ||
                    (this.prevSelectedColor === 'white' && color === 'red' && this.colors.length > 2)) {
                    remove = true;
                }
                if (this.colors.includes('orange')) {
                    colors = 'black-red';
                }

                // Check if the player win
                if ((this.prevSelectedColor === 'orange' && color === 'orange') || (this.prevSelectedColor === 'green' && color === 'green')) {
                    this.gameFinish = true;
                    this.message = 'Awesome! You Won!';
                    this.result = true;
                }
            }
            return [remove, colors];
        },
        playAgain() {
            this.gameFinish = false;
            this.message = null;
            this.result = false;
            this.colors = [];
            this.prevSelectedColor = null;
        }


    }
});